import csv
import game as gm
import fileUtil as fu
import tournamentType as tt
import globals as glb

class Tournament:

    def __init__(self,name : str,year : int,week : int, players : list, tournamentType: tt.TournamentType) -> None:
        self.name:str = name
        self.year:int = year
        self.week:int = week
        self.tournamentType = tournamentType
        self.nParticipant:int = self.tournamentType.nPlayers
        self.isOver : bool = False
        self.winner = None
        self.runnerUp = None
        self.players : list = players
        self.games:list = []
        self.topSeeds = {}
            
    def playRound(self,roundNumber:int) -> None :
        """Play a round"""
        roundToPlay  : list = self.games[-1]

        if roundNumber > 1:
            print("\n-----------------")
            print(glb.roundNames[roundNumber])
            print("-----------------")
            nextRound : list() = []
            toNextRound : str = ""
            for game in roundToPlay:
                self.showGame(game)
                game.play(self.tournamentType.nSets,manual=False)
                game.looser.update(self.findGamesByPlayer(game.looser),roundNumber,self)
                if toNextRound == "":
                    toNextRound = game.winner
                else:
                    newGame = gm.Game(toNextRound,game.winner,self.name,roundNumber)
                    nextRound.append(newGame)
                    toNextRound = ""
            self.games.append(nextRound)
        elif roundNumber == 1 and not self.isOver :
            print("\n-----------------")
            print(glb.roundNames[roundNumber])
            print("-----------------")
            final = roundToPlay[0]  
            self.showGame(final)
            final.play(self.tournamentType.nSets,manual=False)

            self.isOver = True
            self.winner = final.winner
            self.runnerUp = final.playerB if final.winner == final.playerA else final.playerA
            final.looser.update(self.findGamesByPlayer(final.looser),roundNumber,self)
            final.winner.update(self.findGamesByPlayer(final.winner),0,self)
            print(self.winner.name,"WON THE TOURNAMENT")
        else:
            if self.isOver:
                print("This Tournament is over")
    
    def playTournament(self) -> None:
        """Play all the tournament"""
        print("\n-----------------")
        print(self.name," - ",self.year, ".", self.week)
        print("-----------------")

        self.games.append(self.tournamentType.draw(self.players,self.name))
        self.showTopSeed()

        for roundNumber in range(self.tournamentType.nRounds,0,-1):
            self.playRound(roundNumber)

        self.export()

    def showTopSeed(self) -> None :
        """Show the TopSeeds"""
        print("\nTOP SEED : ")
        for seed in range(int(self.nParticipant/4)):
            print(seed+1,":",self.players[seed].name)
            self.topSeeds[self.players[seed].name] = seed+1

    def export(self) -> None:
        filePath = "Tournaments/" + str(self.year) + "/T" + "_" + self.name + ".csv"
        tournamentPath = str(self.year) + "/Games/" + self.name + ".csv"

        with open(filePath,'w',newline='') as f:
            writer = csv.writer(f)
            writer.writerow(["gameId","tournamentName","round","p1ID","p1Name","p2Id","P2Name","p1Score","p2Score"])
            for round in self.games:
                for g in round:
                    writer.writerow(g.toCsv())

        tournamentPath = str(self.year) + "/Tournaments/" + self.name + ".csv"
        with open(tournamentPath,'w',newline='') as g:
            writer = csv.writer(g)
            writer.writerows([
                ['name',self.name],
                ['year',str(self.year)],
                ['week',str(self.week)],
                ['type',self.tournamentType.name],
                ['winner',self.winner.name],
                ['finalist',self.runnerUp.name]
            ])

        yearPath = str(self.year) + "/" + str(self.year) + ".csv"
        with open(yearPath,'a',newline='') as h:
            writer = csv.writer(h)
            writer.writerow([str(self.week),self.name,self.tournamentType.name,self.winner.name,self.runnerUp.name])

        fu.exportXlsTournament(self)
    
    def findGamesByPlayer(self,player):
        gms = []
        for round in self.games:
            for g in round:
                if player == g.playerA or player == g.playerB:
                    gms.append(g)
        return gms

    def showGame(self,game : gm.Game):
        toPrint = "\n#" + str(game.id) + ", "
        if game.playerA.getName() in self.topSeeds.keys():
            toPrint += " (" + str(self.topSeeds[game.playerA.name]) + ") "
        toPrint += game.playerA.getName() + " vs "
        if game.playerB.getName() in self.topSeeds.keys():
            toPrint += " (" + str(self.topSeeds[game.playerB.name]) + ") "
        toPrint += game.playerB.getName() + ", "
        toPrint += str(game.playerA.getElo()-game.playerB.getElo())
        print(toPrint)