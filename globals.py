import tournamentType as tt

year = 2001

roundNames = {
    7 : "64e de Finales",
    6 : "32e de Finales",
    5 : "16e de Finales",
    4 : "8e de Finales",
    3 : "Quarts de Finales",
    2 : "Demi-Finales",
    1 : "Finale"
}

playersCsvAttributes = ['id','name','country','elo','rkPts','rank',
                        'highElo','highRkPts','highRk','eloDiff','rkPtsDiff',
                        'week1','week2','week3','week4','week5',
                        'week6','week7','week8','week9','week10']

# Create Tournament Type
miniType = tt.TournamentType("Mini",0.5,28,2,       [125,90,45,20,10,5])
tourType = tt.TournamentType("Tour",0.75,32,2,      [250,150,90,45,20,10])
champType = tt.TournamentType("Champ",1,32,2,       [500,300,180,90,45,20])
masterType = tt.TournamentType("Master",1.5,64,2,   [1000,600,360,180,90,45,25])
openType = tt.TournamentType("Open",3,128,3,        [2000,1200,720,360,180,90,45,20])
