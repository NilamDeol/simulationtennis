# Simu

---

### Idea

 - 1 attribut(col) / week, val global : nb week
 - Réparer calcul de points
 - Create Template for weeks
 - Export tournament to csv
 - Import Tournament
 - Ideas as gitlab issues ?
 - ~~Game : Add Tid,round~~ *IMPLEMENTED*
 - ~~1 csv / players w/ Game + Elo~~ *GAVE UP*
 - ~~1 csv / players w/ Week + Elo~~ *IMPLEMENTED*
 - ~~Pretty excel, csv to xslx~~ *IMPLEMENTED*
 - ~~Algorithm to calculate the score~~ *IMPLEMENTED*
 - ~~Tournament : Add week into attributes~~ *IMPLEMENTED BUT NOT TESTED*
 - ~~Add nSets in Game call for algo~~ *IMPLEMENTED BUT NOT TESTED*
 - ~~Set git Project~~ *DONE*

---

### 4 Juin

Delete players list type attributes because not handle by csv

### 5 Juin

 - Game : Add tournamentID,round
 - Game : Change Export File
 - Tournament : Add Export
 - Players : Add export to keep result by week

*--> Launch week 4*

 - Tournament : Add export for a year resume
 - Global : Add first global variables (year)

*--> Launch week 5*

### 7 Juin

 - Tournament : Add pretty xls export for tournament

*--> Launch week 6*

 - Game : Start the algo for automatisation of the score

### 8 Juin

 - Tournament : Add week into attributes
 - Game : Add nSets for the call of the algo
 - Git : Put project on gitlab