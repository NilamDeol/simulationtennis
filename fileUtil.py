import csv
from players import Player
from xlwt import Workbook
import globals as glb

def import_csv_file(file_name):
    data = []
    with open(file_name, 'r') as file:
        csv_reader = csv.reader(file)
        for row in csv_reader:
            data.append(row)
    return data

# # Example usage
# csv_file_name = 'data.csv'
# imported_data = import_csv_file(csv_file_name)

def exportPlayers(filePath,players) -> None :

    with open(filePath,'w',newline='') as f:
        writer = csv.DictWriter(f,fieldnames=glb.playersCsvAttributes)
        writer.writeheader()
        for plrs in players:
            writer.writerow({
                'id' : plrs.id,
                'name' : plrs.name,
                'country' : plrs.country,
                'elo' : plrs.elo,
                'rkPts' : plrs.rankPoints,
                'rank' : plrs.rank,
                'highElo' : plrs.highestElo,
                'highRkPts' : plrs.highestRkPts,
                'highRk' : plrs.highestRank,
                'eloDiff': plrs.eloDiff,
                'rkPtsDiff': plrs.rkPtsDiff,
                'week1': plrs.actualSeason[0],
                'week2': plrs.actualSeason[1],
                'week3': plrs.actualSeason[2],
                'week4': plrs.actualSeason[3],
                'week5': plrs.actualSeason[4],
                'week6': plrs.actualSeason[5],
                'week7': plrs.actualSeason[6],
                'week8': plrs.actualSeason[7],
                'week9': plrs.actualSeason[8],
                'week10': plrs.actualSeason[9],
            })
    
def importPlayers(filePath):
    players = []
    with open(filePath, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            player = Player()
            player.set(
                id=int(row['id']),
                name=row['name'],
                country=row['country'],
                elo= float(row['elo']),
                rankPoints= int(row['rkPts']),
                rank = int(row['rank']),
                highestElo= float(row['highElo']),
                highestRkPts= int(row['highRkPts']),
                highestRank= int(row['highRk'])
            )
            player.setSeason(row['week1'],row['week2'],row['week3'],
                             row['week4'],row['week5'],row['week6'],
                             row['week7'],row['week8'],row['week9'],
                             row['week10'])
            players.append(player)
    return players

def exportDraw(filePath,draw):
    with open(filePath,'w',newline='') as f:
        writer = csv.writer(f)
        for i in range(len(draw[0])):
            toSave = []
            for j in range(len(draw)):
                if len(draw[j]) > i:
                    toSave.append(draw[j][i].name)
            writer.writerow(toSave)

def exportXlsTournament(tournament):
    wb = Workbook()
    fileName = str(tournament.year) + '/Tournaments_Pretty/' + tournament.name + '.xls'
    sheet1 = wb.add_sheet(tournament.name)

    ll = (2,3)
    larg = 0
    debut = 0
    ecart = ll[0]
    nbMatch = len(tournament.games[0])

    w = 1

    for round in tournament.games:
        gameId = 0
        for i in range(debut,(ecart+ll[0])*nbMatch+debut,ecart+2):
            game = round[gameId]
            sheet1.write(i,larg,game.playerA.name)
            sheet1.write(i+1,larg,game.playerB.name)
            sheet1.write(i,larg+1,game.playerA.elo)
            sheet1.write(i+1,larg+1,game.playerB.elo)
            sheet1.write(i,larg+2,game.score[0])
            sheet1.write(i+1,larg+2,game.score[1])
            gameId += 1
        
        larg = larg+ll[1]+1
        debut = debut + (ll[0]**w)
        ecart = ecart + (ll[0]**(w+1))
        nbMatch = int(nbMatch/2)

        w += 1
        
    wb.save(fileName)