import util
import csv

class Player:

    def __init__(self):
        pass

    def create(self,name,country):
        self.id : -1

        self.name = name
        self.country = country
        self.elo = 0.0
        self.rankPoints = 0
        self.highestElo = 0.0
        self.highestRkPts = 0
        self.highestRank = None
        self.eloDiff:float = 0.0
        self.rkPtsDiff:int = 0
        self.actualSeason = []
        self.titles = []
        self.games = []

    def set(self, id, name, country, elo, rank, rankPoints, highestElo, highestRkPts, highestRank):
        self.id = id
        self.name = name
        self.country = country
        self.elo = elo
        self.rankPoints = rankPoints
        self.rank = rank
        self.highestElo = highestElo
        self.highestRkPts = highestRkPts
        self.highestRank = highestRank
        self.eloDiff:float = 0.0
        self.rkPtsDiff:int = 0
        self.actualSeason = []
        self.titles = []
        self.games = []

    def setSeason(self,a,z,e,r,t,y,u,i,o,p):
        self.actualSeason=[a,z,e,r,t,y,u,i,o,p]
        for i in range(len(self.actualSeason)):
            self.actualSeason[i] = int(self.actualSeason[i])

    def update(self,gms,round,tournament) -> None:
        newPoints = tournament.tournamentType.rndPts[round]
        oldElo = self.elo

        coefficient = tournament.tournamentType.coef

        #Update Elo
        for g in gms:
            self.games.append(g.id)
            if g.playerA == self:
                self.elo += util.calculateElo(self.elo,g.playerB.elo,(g.winner==self))*coefficient
            else:
                self.elo += util.calculateElo(self.elo,g.playerA.elo,(g.winner==self))*coefficient

        if self.elo > self.highestElo:
            self.highestElo = self.elo

        #Update Rank Points
        self.actualSeason[tournament.week-1] = newPoints
        self.rankPoints = self.calculatePointsFromSeason()

        #Update Diff
        self.rkPtsDiff = newPoints
        self.eloDiff = self.elo - oldElo

        #Update Highest
        if self.rankPoints > self.highestRkPts:
            self.highestRkPts = self.rankPoints

        if self.rank < self.highestRank:
            self.highestRank = self.rank

        playersPath = 'Players/' + str(self.id) + "_" + self.name + ".csv"
        with open(playersPath,'a',newline='') as f:
            writer = csv.writer(f)
            writer.writerow([tournament.year,tournament.week,tournament.name,round,len(gms),self.eloDiff,
                             self.rkPtsDiff,self.elo,self.rankPoints])

    def calculatePointsFromSeason(self) -> float:
        rkPts: float = 0.0
        for x in self.actualSeason:
            rkPts += x
        return rkPts
    
    def __str__(self) -> str :
        pass

    def getName(self) -> str :
        return self.name
    
    def getElo(self) -> float:
        return self.elo