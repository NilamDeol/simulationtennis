# Simu

## Program

| Week | 2001               | 2002                      |
|:----:|:------------------:|:-------------------------:|
| 1    | Open               | Open                      |
| 2    | Tour x4            | Tour x5                   |
| 3    | Master x1, Tour x2 | Champ x4                  |
| 4    | Champ x4           | Master x1, Mini x2        |
| 5    |                    | Champ x2, Tour x2, Mini   |
| 6    | Master x1, Tour x2 | Master x1, Mini x3        |
| 7    |                    | Master x1, Mini x2        |
| 8    |                    | Tour x5                   |
| 9    | Champ x4           | Champ x4                  |
| 10   | Open               | Open                      |

### Week 1

| Name          | Level | nPlayers  | Last Winner   |
|:-------------:|:-----:|:---------:|:-------------:|
| Austrian Open | 2000  | 128       | Innsbruck     |

Players : 128 - 32

### Week 2

| Name              | Level | nPlayers  | Last Winner   |
|:-----------------:|:-----:|:---------:|:-------------:|
| Buenos Aires Tour | 250   | 32        | Racing        |
| Tour 1            | 250   | 32        | Innsbruck     |
| Honved Tour       | 250   | 32        | Honved        |
| Norwich Tour      | 250   | 32        | Norwich       |
| Tour 2            | 250   | 32        |               |

Players : 160 - 0

### Week 3

| Name              | Level | nPlayers  | Last Winner   |
|:-----------------:|:-----:|:---------:|:-------------:|
| Tyrol Champ       | 500   | 32        | Innsbruck     |
| Cska Champ        | 500   | 32        | Cska          |
| Lille Champ       | 500   | 32        | Lille         |
| Champ 1           | 500   | 32        | Olympiacos    |

Players : 128 - 32

### Week 4

| Name              | Level | nPlayers  | Last Winner   |
|:-----------------:|:-----:|:---------:|:-------------:|
| Instanbul Master  | 1000  | 64        | Galatasaray   |
| Mini 1            | 125   | 28        |               |
| Mini 2            | 125   | 28        |               |

Players : 120 - 40

### Week 5

| Name      | Level | nPlayers  | Last Winner   |
|:---------:|:-----:|:---------:|:-------------:|
| Champ 2   | 500   | 32        |               |
| Champ 3   | 500   | 32        |               |
| Tour 3    | 250   | 32        |               |
| Tour 4    | 250   | 32        |               |
| Mini 3    | 125   | 28        |               |

Players : 156 - 4

### Week 6

| Name              | Level | nPlayers  | Last Winner   |
|:-----------------:|:-----:|:---------:|:-------------:|
| Avellaneda Master | 1000  | 64        | Racing        |
| Mini 4            | 125   | 28        |               |
| Mini 5            | 125   | 28        |               |
| Mini 6            | 125   | 28        |               |

Players : 148 - 12

### Week 7

| Name              | Level | nPlayers  | Last Winner   |
|:-----------------:|:-----:|:---------:|:-------------:|
| Master 3          | 1000  | 64        |               |
| Mini 7            | 125   | 28        |               |
| Mini 8            | 125   | 28        |               |

Players : 120 - 40

### Week 8

| Name          | Level | nPlayers  | Last Winner   |
|:-------------:|:-----:|:---------:|:-------------:|
| Tour 5        | 250   | 32        | Racing        |
| Tour 6        | 250   | 32        | Nagoya        |
| Debrecen Tour | 250   | 32        | Debrecen      |
| Tour 7        | 250   | 32        | Lille         |
| Tour 8        | 250   | 32        |               |

Players : 160 - 0

### Week 9

| Name          | Level | nPlayers  | Last Winner   |
|:-------------:|:-----:|:---------:|:-------------:|
| Champ 4       | 500   | 32        | Racing        |
| Champ 5       | 500   | 32        | Norwich       |
| Athens Champ  | 500   | 32        | Olympiacos    |
| Champ 6       | 500   | 32        | Nagoya        |

Players : 128 - 32

### Week 10

| Name          | Level | nPlayers  | Last Winner   |
|:-------------:|:-----:|:---------:|:-------------:|
| Japan Open    | 2000  | 128       | Nagoya        |

Players : 128 - 32