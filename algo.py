import random

eloDifference = {
    500 : 0.25,
    400 : 0.30,
    300 : 0.35,
    200 : 0.40,
    150 : 0.425,
    100 : 0.45,
    50  : 0.475,
    25  : 0.4875,
    0   : 0.5,
}

def aSet(coef,isA):
    over = False
    scA = 0
    scB = 0
    if isA:
        while not over:
            rmd = random.random()
            if rmd > coef:
                scA += 1
            else:
                scB += 1
            over = isOver(scA,scB)
    else:
        while not over:
            rmd = random.random()
            if rmd > coef:
                scB += 1
            else:
                scA += 1
            over = isOver(scA,scB)
    return (scA,scB)

def isOver(sc1,sc2):
    if (sc1 == 6 and sc2 <= 4) or (sc2 == 6 and sc1 <= 4):
        return True
    elif (sc1 == 7 and sc2 <= 5) or (sc2 == 7 and sc1 <= 5):
        return True
    elif (sc1 == 7 and sc2 == 6) or (sc2 == 7 and sc1 == 6):
        return True
    return False

def nSets(eloA,eloB,nSets):
    x,y = 0,0
    sets = []
    isA = True

    valAbs = abs(eloA-eloB)
    coef = 0.5
    if eloB > eloA:
        isA = False

    for i in eloDifference.keys():
        if valAbs>i:
            coef = eloDifference[i]
            break

    while (x!=nSets and y!=nSets):
        (a,b) = aSet(coef,isA)
        sets.append((a,b))
        if a>b:
            x += 1
        else:
            y += 1
    if x==2:
        return (1,x,y,sets)
    else:
        return (2,x,y,sets)