import random

def setSeeds(plys,count):
    n = len(plys)
    seeds = [i+count for i in range(count)]
    random.shuffle(seeds)
    for i in range(count):
        if i%2 == 1:
            plys[i*(int(n/count))] = seeds[i]
            plys[i*(int(n/count))-1] = seeds[i-1]
    return plys

eloDifference = {
    500 : [0,40,0,-29],
    400 : [0.5,28,0,-20],
    300 : [1,22,-0.5,-16],
    200 : [2,17,-1,-12.5],
    150 : [3,13,-2,-10],
    100 : [4,10,-3,-8],
    50  : [5,8,-4,-7],
    25  : [5.5,7,-4.5,-6],
    -1   : [6,6,-5,-5]
}
        
def calculateElo(eloA,eloB,isWinner):
    indexEloDiff = 0 if eloB < eloA else 1
    if not isWinner:
        indexEloDiff += 2
    diffAbs = abs(eloA-eloB)
    for i in eloDifference.keys():
        if diffAbs>i:
            return eloDifference[i][indexEloDiff]