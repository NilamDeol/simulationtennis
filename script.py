import fileUtil as fu
import tournament as to
import tournamentType as tt
import globals as glb
import random

# Create Tournament Type
miniType = tt.TournamentType("Mini",0.5,28,2,       [125,90,45,20,10,5])
tourType = tt.TournamentType("Tour",0.75,32,2,      [250,150,90,45,20,10])
champType = tt.TournamentType("Champ",1,32,2,       [500,300,180,90,45,20])
masterType = tt.TournamentType("Master",1.5,64,2,   [1000,600,360,180,90,45,25])
openType = tt.TournamentType("Open",3,128,3,        [2000,1200,720,360,180,90,45,20])

def weekTest():
    importPathPlayers = 'test/Players_2.0.csv'
    exportPathPlayers = 'test/playersAfter.csv'

    pls = fu.importPlayers(importPathPlayers)

    t250A = []

    # Draw of which players will play which tournaments
    for i in range(0,len(pls),4):
        toDraw = [pls[i],pls[i+1],pls[i+2],pls[i+3]]
        random.shuffle(toDraw)
        t250A.append(toDraw[2])

    fu.exportDraw('test/test_draw.csv',[t250A])

    tour = to.Tournament("Test",glb.year,-1,t250A,tourType)

    thirdWeek = [tour]

    for tour in thirdWeek:
        tour.playTournament()

    fu.exportPlayers(exportPathPlayers,pls)

def week7(year):
    importPathPlayers = str(year) + '/players_6.csv'
    exportPathPlayers = str(year) + '/players_7.csv'

    pls = fu.importPlayers(importPathPlayers)

    o2000A = []

    # Draw of which players will play which tournaments
    for i in range(0,len(pls)):
        o2000A.append(pls[i])

    fu.exportDraw('2001/Draws/Week7_draw.csv',[o2000A])

    openA = to.Tournament("O2",year,7,4,o2000A)

    weekTournament = [openA]

    for tour in weekTournament:
        tour.playTournament()

    fu.exportPlayers(exportPathPlayers,pls)

def week6(year):
    importPathPlayers = str(year) + '/players_5.csv'
    exportPathPlayers = str(year) + '/players_6.csv'

    pls = fu.importPlayers(importPathPlayers)

    t500A = []
    t500B = []
    t500C = []
    t500D = []

    # Draw of which players will play which tournaments
    for i in range(0,len(pls),4):
        toDraw = [pls[i],pls[i+1],pls[i+2],pls[i+3]]
        random.shuffle(toDraw)
        t500A.append(toDraw[0])
        t500B.append(toDraw[1])
        t500C.append(toDraw[2])
        t500D.append(toDraw[3])

    fu.exportDraw('2001/Draws/Week6_draw.csv',[t500A,t500B,t500C,t500D])

    chpA = to.Tournament("C5",year,2,t500A)
    chpB = to.Tournament("C6",year,2,t500B)
    chpC = to.Tournament("C7",year,2,t500C)
    chpD = to.Tournament("C8",year,2,t500D)

    thirdWeek = [chpA,chpB,chpC,chpD]

    for tour in thirdWeek:
        tour.playTournament()

    fu.exportPlayers(exportPathPlayers,pls)

def week5(year):
    importPathPlayers = str(year) + '/players_4.csv'
    exportPathPlayers = str(year) + '/players_5.csv'

    pls = fu.importPlayers(importPathPlayers)

    t1000 = []
    t250A = []
    t250B = []

    # Draw of which players will play which tournaments
    for i in range(0,len(pls),4):
        toDraw = [pls[i],pls[i+1],pls[i+2],pls[i+3]]
        random.shuffle(toDraw)
        t1000.append(toDraw[0])
        t1000.append(toDraw[1])
        t250A.append(toDraw[2])
        t250B.append(toDraw[3])

    fu.exportDraw('2001/Draws/Week5_draw.csv',[t1000,t250A,t250B])

    master1 = to.Tournament("M2",year,3,t1000)
    tour1 = to.Tournament("T7",year,1,t250A)
    tour2 = to.Tournament("T8",year,1,t250B)

    thirdWeek = [master1,tour1,tour2]

    for tour in thirdWeek:
        tour.playTournament()

    fu.exportPlayers(exportPathPlayers,pls)

def week4(year):
    importPathPlayers = str(year) + '/players_3.csv'
    exportPathPlayers = str(year) + '/players_4.csv'

    pls = fu.importPlayers(importPathPlayers)

    t500A = []
    t500B = []
    t500C = []
    t500D = []

    # Draw of which players will play which tournaments
    for i in range(0,len(pls),4):
        toDraw = [pls[i],pls[i+1],pls[i+2],pls[i+3]]
        random.shuffle(toDraw)
        t500A.append(toDraw[0])
        t500B.append(toDraw[1])
        t500C.append(toDraw[2])
        t500D.append(toDraw[3])

    fu.exportDraw('2001/Draws/Week4_draw.csv',[t500A,t500B,t500C,t500D])

    chpA = to.Tournament("C1",year,2,t500A)
    chpB = to.Tournament("C2",year,2,t500B)
    chpC = to.Tournament("C3",year,2,t500C)
    chpD = to.Tournament("C4",year,2,t500D)

    thirdWeek = [chpA,chpB,chpC,chpD]

    for tour in thirdWeek:
        tour.playTournament()

    fu.exportPlayers(exportPathPlayers,pls)

def week3(year):
    importPathPlayers = str(year) + '/players_2.csv'
    exportPathPlayers = str(year) + '/players_3.csv'

    pls = fu.importPlayers(importPathPlayers)

    t1000 = []
    t250A = []
    t250B = []

    # Draw of which players will play which tournaments
    for i in range(0,len(pls),4):
        toDraw = [pls[i],pls[i+1],pls[i+2],pls[i+3]]
        random.shuffle(toDraw)
        t1000.append(toDraw[0])
        t1000.append(toDraw[1])
        t250A.append(toDraw[2])
        t250B.append(toDraw[3])

    fu.exportDraw('2001_Draw/2001_3_draw.csv',[t1000,t250A,t250B])

    master1 = to.Tournament("M1",year,3,t1000)
    tour1 = to.Tournament("T1",year,1,t250A)
    tour2 = to.Tournament("T2",year,1,t250B)

    thirdWeek = [master1,tour1,tour2]

    for tour in thirdWeek:
        tour.playTournament()

    fu.exportPlayers(exportPathPlayers,pls)

def week2(year):
    importPathPlayers = 'players_1.csv'
    exportPathPlayers = 'players_2.csv'

    pls = fu.importPlayers(importPathPlayers)

    tu1 = []
    tu2 = []
    tu3 = []
    tu4 = []

    for i in range(0,len(pls),4):
        toDraw = [pls[i],pls[i+1],pls[i+2],pls[i+3]]
        random.shuffle(toDraw)
        tu1.append(toDraw[0])
        tu2.append(toDraw[1])
        tu3.append(toDraw[2])
        tu4.append(toDraw[3])

    fu.exportDraw4('tmp.csv',[tu1,tu2,tu3,tu4])

    champs1 = to.Tournament("C1",year,1,tu1)
    champs2 = to.Tournament("C2",year,1,tu2)
    champs3 = to.Tournament("C3",year,1,tu3)
    champs4 = to.Tournament("C4",year,1,tu4)

    secondWeek = [champs1,champs2,champs3,champs4]

    for tour in secondWeek:
        tour.playTournament()

    fu.exportPlayers(exportPathPlayers,pls)

def week1(year):
    # frenchOpen = to.Tournament("FrenchOpen",2001,4,pls)
    # frenchOpen.draw()
    # frenchOpen.showTopSeed()
    # frenchOpen.playRound()
    # frenchOpen.playRound()
    # frenchOpen.playRound()
    # frenchOpen.playRound()
    # frenchOpen.playRound()
    # frenchOpen.playRound()
    # frenchOpen.playRound()
    # frenchOpen.export()
    pass  

weekTest()
# week7(2001)