import players as pl
import algo

class Game:

    Game_id : int = 0

    def __init__(self,playerA : pl.Player,playerB : pl.Player,tourName : str,rnd : int) -> None:
        # Id of the game in the week (not on the tournament)
        Game.Game_id += 1
        self.id : int = Game.Game_id

        self.playerA : pl.Player = playerA
        self.playerB = pl.Player = playerB
        self.isPlayed = False
        self.winner : pl.Player = None
        self.looser : pl.Player = None
        self.score = None
        self.tournamentName : int = tourName
        self.round:int = rnd

    def setGame(self,score) -> None :
        self.score = score
        if score[0] > score[1]:
            self.winner = self.playerA
            self.looser = self.playerB
        else:
            self.winner = self.playerB
            self.looser = self.playerA
        self.isPlayed = True

    def play(self,nSets,manual = True) -> None :
        if self.isPlayed:
            print("ERROR : This game has already been played.")
            pass
        elif not manual:
            # _ = input("Launch ?")
            sc = algo.nSets(self.playerA.elo,self.playerB.elo,nSets)
            print(sc[3])
            self.setGame((sc[1],sc[2]))
            print(self.winner.name,"won the game")
        else:
            scA = inputScore("Score " + self.playerA.getName() + "? ")
            scB = inputScore("Score " + self.playerB.getName() + "? ")
            self.setGame((scA,scB))
            print(self.winner.name,"won the game")

    def __str__(self) -> str:
        toPrint = "\n#" + str(self.id) + ", " + self.playerA.getName() + " vs " + self.playerB.getName() + ", " + str(self.playerA.getElo()-self.playerB.getElo())
        if self.isPlayed:
            toPrint += " : " + self.score[0] + "-" + self.score[1]
        return toPrint
    
    def toCsv(self):
        """To write in csv (for tournament)"""
        return [self.id,self.tournamentName,self.round,self.playerA.id,self.playerA.name,self.playerB.id,self.playerB.name,self.score[0],self.score[1]]


def inputScore(message):
    x = input(message)
    while x not in ["0","1","2","3"]:
        x = input(message)
    return int(x)