import random
import fileUtil as fu
import tournament as to
import globals as glb

def playersFilePath(weekNumber:int, yearNumber:int)->str:
    return str(yearNumber) + '/Players_' + str(yearNumber%2000) + '.' + str(weekNumber) + '.csv'

def drawFilePath(weekNumber:int, yearNumber:int)->str:
    return str(yearNumber) + '/Draws/Draws_' + str(yearNumber%2000) + '.' + str(weekNumber) + '.csv'

def setWeek(weekNumber:int, yearNumber:int):
    #Path
    importPath = playersFilePath(weekNumber-1,yearNumber)
    exportPath = playersFilePath(weekNumber,yearNumber)

    #Players
    allPlayers = fu.importPlayers(importPath)

    #Tournament
    tournaments = setWeekOneOpen(allPlayers,drawFilePath(1,yearNumber))

    #Play Tournament
    for tournament in tournaments:
        tournament.playTournament()

    fu.exportPlayers(exportPath,allPlayers)


def setWeekOneOpen(allPlayers:list, drawPath:str, yearNumber:int)->list:

    #Draw
    seeds = allPlayers[:32]
    others = allPlayers[32:]
    random.shuffle(others)
    players = seeds + others
    fu.exportDraw(drawPath,[players])

    #Tournament
    austrianOpen = to.Tournament("Austrian Open",yearNumber,1,players,glb.openType)
    allTournaments = [austrianOpen]

    return allTournaments