import random
import game as gm

class TournamentType:

    def __init__(self,name:str,coef:float,nPlayers:int,nSets:int,rndPts:list):
        self.name = name
        self.coef = coef
        self.nPlayers = nPlayers
        self.nSets = nSets
        self.rndPts = rndPts
        self.nRounds = len(rndPts)-1
        self.nSeeds = pow(2,self.nRounds-2)
        self.nBye = (pow(2,self.nRounds)-self.nPlayers)

    def setSeeds(self,plys:list,count:int,isBye:bool=False):
        n = len(plys)
        seeds = [i+count for i in range(count)]
        random.shuffle(seeds)
        for i in range(count):
            if i%2 == 1:
                plys[i*(int(n/count))] = seeds[i]
                plys[i*(int(n/count))-1] = seeds[i-1]
                if isBye:
                    plys[i*(int(n/count))+1] = -2
                    plys[i*(int(n/count))-2] = -2
        return plys

    def draw(self,players:list,tournamentName:str) -> list :
        """Set the draw for the tournament"""
        indexes : list = [i for i in range(self.nPlayers)]
        firstRound = [-1 for _ in range(pow(2,self.nRounds))]

        firstRound[0] = 0
        firstRound[-1] = 1
        if self.nBye >= 2:
            firstRound[1] = -2
            firstRound[-2] = -2

        indexes.remove(0)
        indexes.remove(1)

        i = self.nSeeds / 2
        k = self.nBye/2
        while i != 1:
            if k >= 1:
                firstRound = self.setSeeds(firstRound,int(i),True)
            else:
                firstRound = self.setSeeds(firstRound,int(i),False)
            for j in range(int(i),int(i)*2):
                indexes.remove(j)
            i = i/2
            k = k/2


        random.shuffle(indexes)
        for i in range(len(firstRound)):
            if firstRound[i] == -1:
                firstRound[i] = indexes[0]
                indexes.remove(indexes[0])
            if firstRound[i] == -2:
                firstRound[i] = -1
        
        firstRoundGames = []
        for i in range(0,len(firstRound),2):
            firstPlayer = "Bye" if firstRound[i] == -1 else players[firstRound[i]]
            secondPlayer = "Bye" if firstRound[i+1] == -1 else players[firstRound[i+1]]
            aGame = gm.Game(firstPlayer,secondPlayer,tournamentName,self.nPlayers)
            firstRoundGames.append(aGame)
        
        return firstRoundGames